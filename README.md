# ansible-playbooks-conductor-workflows-gcube

The playbooks for selectively deploy Conductor based workflows related to gcube internal activities such as user management

Examples:

The following example deploys user-management workflows that coordinate LR62 portal and Keycloak IAM to the conductor server running in dev environment (http://conductor-dev.int.d4science.net). 
`./deploy.sh user-management-workflows`

The following example deploys user-management workflows that coordinate LR62 portal and Keycloak IAM to the conductor server running in pre environment.
`./deploy.sh user-management-workflows -e conductor_server=https://conductor.pre.d4science.org`

A script `deploy-pre.sh` has been created in order to simplify deployment to gCube pre-production environment.
