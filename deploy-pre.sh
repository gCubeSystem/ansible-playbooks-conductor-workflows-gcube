#!/bin/bash

./deploy.sh user-management-workflows -e keycloak_host=https://accounts.pre.d4science.org/auth -e liferay=https://pre.d4science.org/api/jsonws -e conductor_server=https://conductor.pre.d4science.org/api -e keycloak_auth=b0721aac-c70c-4aa3-9613-1f901b20ae24 -e keycloak_auth_master=002a4ea6-df4e-4280-95c0-5886d22498c9 -e root_vo=%2Fpred4s -e ic_proxy=https://node10-d-d4s.d4science.org -e storagehub=https://api.pre.d4science.org
