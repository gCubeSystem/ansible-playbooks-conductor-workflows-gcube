# Remote ansible roles. First the ones globally used
ansible-galaxy install --force-with-deps -r "$1/requirements.yaml"

ansible-playbook "$1/playbook.yaml" "${@:2}"
